import React from 'react'
import classnames from 'classnames'
import injectSheet from 'react-jss'
import { Label } from './../index'

const styles = {
  root: {
    position: 'relative',
  },
  helper: {
    position: 'relative',
  },
  select: {
    width: '100%',
    height: '38px',
    padding: '9px 16px',
    border: '1px solid #DDD',
    borderRadius: '16px',
    fontSize: '14px',
    outline: 'none',
    '&:hover, &:focus': {
      outline: 'none',
    },
    '&:disabled': {
      opacity: 0.5,
    },
    '&.error': {
      borderColor: 'red',
    },
  },
  option: {
    '&:disabled': {
      opacity: 0.5,
    },
  },
}

const Select = ({
  classes,
  className,
  getRef,
  options,
  name,
  label,
  defaultValue,
  disabled,
  error,
  onChange,
}) => (
  <div className={classes.root}>
    {label && <Label htmlFor={name}>{label}</Label>}
    <div className={classes.helper}>
      <select
        ref={getRef}
        name={name}
        className={classnames(classes.select, className, { error })}
        defaultValue={defaultValue}
        disabled={disabled}
        onChange={onChange}
        value={undefined}
      >
        <option style={{ display: 'none' }}></option>
        <option value="" disabled>
          Выберите значение
        </option>
        {options.map(({ id, description, ...rest }) => (
          <option value={id} key={id} {...rest}>
            {description}
          </option>
        ))}
      </select>
    </div>
  </div>
)

export default injectSheet(styles)(Select)
