import React from 'react'
import Icons from './img.svg'

export default ({ className, iconName }) => 
	<svg className={className}>
	  <use xlinkHref={`${Icons}#${iconName}`} />
	</svg>