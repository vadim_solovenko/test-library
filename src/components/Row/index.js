import React from 'react'
import classnames from 'classnames'
import injectSheet from 'react-jss'

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    marginRight: '-10px',
    marginLeft: '-10px',
    position: 'relative',
    '& + &': {
      marginTop: '15px',
    },
  },
}

const Row = ({ 
	classes,
  className,
  children
}) => 
  <div className={classnames(classes.root, className)}>
    {children}
  </div>

export default injectSheet(styles)(Row)