import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";

import Overlay from "./index";

export const text = "Hello Label";

export const actions = {
  onClick: action("onClick"),
};

storiesOf("Overlay", module)
  .add("default", () => <Overlay>{text}</Overlay>)
  .add("small", () => <Overlay size="small">{text}</Overlay>);
