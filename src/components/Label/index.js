import React from 'react'
import classnames from 'classnames'
import injectSheet from 'react-jss'

const styles = {
  root: {
    display: 'block',
    fontSize: '14px',
    lineHeight: '18px',
    color: '#000',
    fontWeight: 500,
    marginBottom: '4px',
  },
}

const Label = ({ 
	classes,
  className,
  htmlFor,
  children
}) => 
  <label htmlFor={htmlFor} className={classnames(classes.root, className)}>
    {children}
  </label>

export default injectSheet(styles)(Label)