const h1 = {
  paddingBottom: '20px',
  fontWeight: 500,
  fontSize: '22px',
  lineHeight: '26px',
  color: '#000',
}

const title = {
  fontFamily: 'Montserrat',
  fontWeight: 500,
  fontSize: '14px',
  lineHeight: '22px',
  letterSpacing: '0.05em',
  textTransform: 'uppercase',
}

const button = {
  fontFamily: 'Montserrat',
  borderRadius: '8px',
  border: 'none',
  color: '#fff',
  fontSize: '14px',
  lineHeight: '26px',
  padding: '6px 30px',
  cursor: 'pointer',
  outline: 'none',
  textAlign: 'center',
  display: 'inline-block',
  verticalAlign: 'top',
  textDecoration: 'none',
  transition: 'all 300ms ease',
  '&:hover': {
    color: '#fff',
    textDecoration: 'none',
    boxShadow: '0 2px 4px #ccc',
    outline: 'none',
  },
  '&:focus': {
    outline: 'none',
  },
  '&:disabled': {
    opacity: 0.5,
    cursor: 'default',
  },
}

const addButton = {
  ...button,
  background: '#22C7B5',
  borderColor: '#22C7B5',
  '&:active': {
    background: '#199083',
    borderColor: '#199083',
  },
  '&.disabled': {
    cursor: 'default',
    '&:hover': {
      boxShadow: 'none',
    },
    '&:active': {
      background: '#22C7B5',
      borderColor: '#22C7B5',
      boxShadow: 'none',
    },
  },
}

const removeButton = {
  ...button,
  background: '#b8b8b8',
  borderColor: '#b8b8b8',
  '&:hover': {
    background: '#F45F5F',
    borderColor: '#F45F5F',
  },
}

const cancelButton = {
  ...button,
  background: '#504E5B',
  borderColor: '#504E5B',
  '&:hover': {
    background: '#6A6873',
    borderColor: '#6A6873',
  },
  '&:active': {
    background: '#504E5B',
    borderColor: '#504E5B',
  },
}

const table = {
  marginTop: '20px',
  '& td': {
    verticalAlign: 'top',
    '& > div': {
      lineHeight: '26px',
      paddingTop: '6px',
      paddingBottom: '6px',
    },
  },
  '& tr': {
    borderTop: '1px solid #dee2e6',
  },
}

export default {
  h1,
  title,
  addButton,
  removeButton,
  cancelButton,
  table,
}