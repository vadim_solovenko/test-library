"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactJss = _interopRequireDefault(require("react-jss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  '@keyframes bounce': {
    '0%': {
      transform: 'scale(0)'
    },
    '50%': {
      transform: 'scale(1)'
    },
    '100%': {
      transform: 'scale(0)'
    }
  },
  root: {
    width: '26px',
    height: '26px',
    margin: [0, 'auto'],
    position: 'relative'
  },
  bounce: {
    width: '100%',
    height: '100%',
    borderRadius: '50%',
    position: 'absolute',
    top: 0,
    left: 0,
    opacity: 0.4,
    animation: '$bounce 2.0s infinite ease-in-out'
  },
  bounce1: {
    background: '#000'
  },
  bounce2: {
    background: '#FFF',
    animationDelay: '-1.0s'
  }
};

var Spinner = function Spinner(_ref) {
  var classes = _ref.classes;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)(classes.bounce, classes.bounce1)
  }), /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)(classes.bounce, classes.bounce2)
  }));
};

var _default = (0, _reactJss.default)(styles)(Spinner);

exports.default = _default;