"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactJss = _interopRequireDefault(require("react-jss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    marginRight: '-10px',
    marginLeft: '-10px',
    position: 'relative',
    '& + &': {
      marginTop: '15px'
    }
  }
};

var Row = function Row(_ref) {
  var classes = _ref.classes,
      className = _ref.className,
      children = _ref.children;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)(classes.root, className)
  }, children);
};

var _default = (0, _reactJss.default)(styles)(Row);

exports.default = _default;