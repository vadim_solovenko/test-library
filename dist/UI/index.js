"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var h1 = {
  paddingBottom: '20px',
  fontWeight: 500,
  fontSize: '22px',
  lineHeight: '26px',
  color: '#000'
};
var title = {
  fontFamily: 'Montserrat',
  fontWeight: 500,
  fontSize: '14px',
  lineHeight: '22px',
  letterSpacing: '0.05em',
  textTransform: 'uppercase'
};
var button = {
  fontFamily: 'Montserrat',
  borderRadius: '8px',
  border: 'none',
  color: '#fff',
  fontSize: '14px',
  lineHeight: '26px',
  padding: '6px 30px',
  cursor: 'pointer',
  outline: 'none',
  textAlign: 'center',
  display: 'inline-block',
  verticalAlign: 'top',
  textDecoration: 'none',
  transition: 'all 300ms ease',
  '&:hover': {
    color: '#fff',
    textDecoration: 'none',
    boxShadow: '0 2px 4px #ccc',
    outline: 'none'
  },
  '&:focus': {
    outline: 'none'
  },
  '&:disabled': {
    opacity: 0.5,
    cursor: 'default'
  }
};

var addButton = _objectSpread(_objectSpread({}, button), {}, {
  background: '#22C7B5',
  borderColor: '#22C7B5',
  '&:active': {
    background: '#199083',
    borderColor: '#199083'
  },
  '&.disabled': {
    cursor: 'default',
    '&:hover': {
      boxShadow: 'none'
    },
    '&:active': {
      background: '#22C7B5',
      borderColor: '#22C7B5',
      boxShadow: 'none'
    }
  }
});

var removeButton = _objectSpread(_objectSpread({}, button), {}, {
  background: '#b8b8b8',
  borderColor: '#b8b8b8',
  '&:hover': {
    background: '#F45F5F',
    borderColor: '#F45F5F'
  }
});

var cancelButton = _objectSpread(_objectSpread({}, button), {}, {
  background: '#504E5B',
  borderColor: '#504E5B',
  '&:hover': {
    background: '#6A6873',
    borderColor: '#6A6873'
  },
  '&:active': {
    background: '#504E5B',
    borderColor: '#504E5B'
  }
});

var table = {
  marginTop: '20px',
  '& td': {
    verticalAlign: 'top',
    '& > div': {
      lineHeight: '26px',
      paddingTop: '6px',
      paddingBottom: '6px'
    }
  },
  '& tr': {
    borderTop: '1px solid #dee2e6'
  }
};
var _default = {
  h1: h1,
  title: title,
  addButton: addButton,
  removeButton: removeButton,
  cancelButton: cancelButton,
  table: table
};
exports.default = _default;