"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actions = exports.text = void 0;

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@storybook/react");

var _addonActions = require("@storybook/addon-actions");

var _index = _interopRequireDefault(require("./index"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var text = "Hello Label";
exports.text = text;
var actions = {
  onClick: (0, _addonActions.action)("onClick")
};
exports.actions = actions;
(0, _react2.storiesOf)("Overlay", module).add("default", function () {
  return /*#__PURE__*/_react.default.createElement(_index.default, null, text);
}).add("small", function () {
  return /*#__PURE__*/_react.default.createElement(_index.default, {
    size: "small"
  }, text);
});