"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

require("./overlay.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Overlay = function Overlay(_ref) {
  var _classnames;

  var size = _ref.size,
      inverse = _ref.inverse,
      transparent = _ref.transparent,
      className = _ref.className;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames2.default)("preloader", (_classnames = {}, _defineProperty(_classnames, "preloader--".concat(size), size !== "default"), _defineProperty(_classnames, "preloader--inverse", inverse), _defineProperty(_classnames, "preloader--transparent", transparent), _classnames), className)
  }, /*#__PURE__*/_react.default.createElement("svg", {
    viewBox: "-2000 -1000 4000 2000"
  }, /*#__PURE__*/_react.default.createElement("path", {
    id: "inf",
    d: "M354-354A500 500 0 1 1 354 354L-354-354A500 500 0 1 0-354 354z"
  }), /*#__PURE__*/_react.default.createElement("use", {
    xlinkHref: "#inf",
    strokeDasharray: "1570 5143",
    strokeDashoffset: "6713px"
  })));
};

Overlay.propTypes = {
  size: _propTypes.default.string,
  inverse: _propTypes.default.bool
};
Overlay.defaultProps = {
  size: "default",
  inverse: false
};
var _default = Overlay;
exports.default = _default;