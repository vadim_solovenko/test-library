"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _img = _interopRequireDefault(require("./img.svg"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(_ref) {
  var className = _ref.className,
      iconName = _ref.iconName;
  return /*#__PURE__*/_react.default.createElement("svg", {
    className: className
  }, /*#__PURE__*/_react.default.createElement("use", {
    xlinkHref: "".concat(_img.default, "#").concat(iconName)
  }));
};

exports.default = _default;