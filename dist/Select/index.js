"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactJss = _interopRequireDefault(require("react-jss"));

var _index = require("./../index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var styles = {
  root: {
    position: 'relative'
  },
  helper: {
    position: 'relative'
  },
  select: {
    width: '100%',
    height: '38px',
    padding: '9px 16px',
    border: '1px solid #DDD',
    borderRadius: '16px',
    fontSize: '14px',
    outline: 'none',
    '&:hover, &:focus': {
      outline: 'none'
    },
    '&:disabled': {
      opacity: 0.5
    },
    '&.error': {
      borderColor: 'red'
    }
  },
  option: {
    '&:disabled': {
      opacity: 0.5
    }
  }
};

var Select = function Select(_ref) {
  var classes = _ref.classes,
      className = _ref.className,
      getRef = _ref.getRef,
      options = _ref.options,
      name = _ref.name,
      label = _ref.label,
      defaultValue = _ref.defaultValue,
      disabled = _ref.disabled,
      error = _ref.error,
      onChange = _ref.onChange;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: classes.root
  }, label && /*#__PURE__*/_react.default.createElement(_index.Label, {
    htmlFor: name
  }, label), /*#__PURE__*/_react.default.createElement("div", {
    className: classes.helper
  }, /*#__PURE__*/_react.default.createElement("select", {
    ref: getRef,
    name: name,
    className: (0, _classnames.default)(classes.select, className, {
      error: error
    }),
    defaultValue: defaultValue,
    disabled: disabled,
    onChange: onChange,
    value: undefined
  }, /*#__PURE__*/_react.default.createElement("option", {
    style: {
      display: 'none'
    }
  }), /*#__PURE__*/_react.default.createElement("option", {
    value: "",
    disabled: true
  }, "\u0412\u044B\u0431\u0435\u0440\u0438\u0442\u0435 \u0437\u043D\u0430\u0447\u0435\u043D\u0438\u0435"), options.map(function (_ref2) {
    var id = _ref2.id,
        description = _ref2.description,
        rest = _objectWithoutProperties(_ref2, ["id", "description"]);

    return /*#__PURE__*/_react.default.createElement("option", _extends({
      value: id,
      key: id
    }, rest), description);
  }))));
};

var _default = (0, _reactJss.default)(styles)(Select);

exports.default = _default;