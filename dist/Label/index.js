"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactJss = _interopRequireDefault(require("react-jss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'block',
    fontSize: '14px',
    lineHeight: '18px',
    color: '#000',
    fontWeight: 500,
    marginBottom: '4px'
  }
};

var Label = function Label(_ref) {
  var classes = _ref.classes,
      className = _ref.className,
      htmlFor = _ref.htmlFor,
      children = _ref.children;
  return /*#__PURE__*/_react.default.createElement("label", {
    htmlFor: htmlFor,
    className: (0, _classnames.default)(classes.root, className)
  }, children);
};

var _default = (0, _reactJss.default)(styles)(Label);

exports.default = _default;